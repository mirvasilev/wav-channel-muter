#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]) {
  // Check if option "-s" is provided to disable logging
  int silent = 0;
  for (int i = 1; i < argc; i++) {
    if (strcmp(argv[i], "-s") == 0) {
      silent = 1;
      break;
    }
  }

  // Check if option "-f" is provided to read WAV file path from argument
  char *wav_file_path = NULL; // Initialize to NULL
  for (int i = 1; i < argc - 1; i++) {
    if (strcmp(argv[i], "-file") == 0) {
      wav_file_path = argv[i + 1];
      break;
    }
  }

  // If WAV file path is not provided, read it from stdin
  if (wav_file_path == NULL) {
    wav_file_path = malloc(256 * sizeof(char));
    printf("Please enter WAV file path: ");
    scanf("%s", wav_file_path);
  }

  // Open WAV file for reading
  FILE *wav_file = fopen(wav_file_path, "rb");
  if (wav_file == NULL) {
    fprintf(stderr, "Error: Could not open WAV file %s for reading\n",
            wav_file_path);
    exit(1);
  }

  // Read WAV file header
  char chunk_id[4];
  int chunk_size;
  char format[4];
  char subchunk1_id[4];
  int subchunk1_size;
  short audio_format;
  short num_channels;
  int sample_rate;
  int byte_rate;
  short block_align;
  short bits_per_sample;
  char subchunk2_id[4];
  int subchunk2_size;

  fread(chunk_id, sizeof(char), 4, wav_file);
  fread(&chunk_size, sizeof(int), 1, wav_file);
  fread(format, sizeof(char), 4, wav_file);

  // Read Audio Format
  fread(subchunk1_id, sizeof(char), 4, wav_file);
  fread(&subchunk1_size, sizeof(int), 1, wav_file);
  fread(&audio_format, sizeof(short), 1, wav_file);
  fread(&num_channels, sizeof(short), 1, wav_file);
  fread(&sample_rate, sizeof(int), 1, wav_file);
  fread(&byte_rate, sizeof(int), 1, wav_file);
  fread(&block_align, sizeof(short), 1, wav_file);
  fread(&bits_per_sample, sizeof(short), 1, wav_file);

  // Print number of channels
  if (!silent) {
    printf("Number of channels: %d\n", num_channels);
  }

  fread(subchunk2_id, sizeof(char), 4, wav_file);
  fread(&subchunk2_size, sizeof(int), 1, wav_file);

  // Create file to write WAV output
  FILE *wav_file_out = fopen("output.wav", "wb");
  if (wav_file_out == NULL) {
    fprintf(stderr, "Error: Could not create output WAV file\n");
    exit(1);
  }

  // Generate silence array
  int num_samples = subchunk2_size / block_align;
  short *silence = malloc(num_samples * sizeof(short));
  for (int i = 0; i < num_samples; i++) {
    silence[i] = 0;
  }

  int silent_channels[num_channels];
  int num_silent_channels = 0;
  for (int i = 0; i < num_channels; i++) {
    silent_channels[i] = 0;
  }

  for (int i = 1; i < argc - 1; i++) {
    if (strcmp(argv[i], "-silent") == 0) {
      num_silent_channels = atoi(argv[++i]);
      for (int j = 0; j < num_silent_channels; j++) {
        silent_channels[j] = atoi(argv[++i]);
      }
    }
  }

  if (num_silent_channels > 0) {
    if (!silent) {
      printf("Silencing Channels: ");
      for (int i = 0; i < num_silent_channels; i++) {
        printf("%d ", silent_channels[i]);
      }
      printf("\n");
    }
  }

  // Write WAV file header
  fwrite(chunk_id, sizeof(char), 4, wav_file_out);
  fwrite(&chunk_size, sizeof(int), 1, wav_file_out);
  fwrite(format, sizeof(char), 4, wav_file_out);
  fwrite(subchunk1_id, sizeof(char), 4, wav_file_out);
  fwrite(&subchunk1_size, sizeof(int), 1, wav_file_out);
  fwrite(&audio_format, sizeof(short), 1, wav_file_out);
  fwrite(&num_channels, sizeof(short), 1, wav_file_out);
  fwrite(&sample_rate, sizeof(int), 1, wav_file_out);
  fwrite(&byte_rate, sizeof(int), 1, wav_file_out);
  fwrite(&block_align, sizeof(short), 1, wav_file_out);
  fwrite(&bits_per_sample, sizeof(short), 1, wav_file_out);
  fwrite(subchunk2_id, sizeof(char), 4, wav_file_out);
  fwrite(&subchunk2_size, sizeof(int), 1, wav_file_out);

  // Read and write all sound data, muting channels if necessary
  short data[block_align];

  for (int i = 0; i < num_samples; i++) {
    fread(data, sizeof(short), block_align / sizeof(short), wav_file);

    for (int j = 0; j < num_channels; j++) {
      if (silent_channels[j] != 1) {
        fwrite(&data[j], sizeof(short), 1, wav_file_out);
      } else {
        fwrite(silence, sizeof(short), 1, wav_file_out);
      }
    }
  }

  // Close input and output WAV files
  fclose(wav_file);
  fclose(wav_file_out);

  // Free memory allocated for silence array
  free(silence);

  if (!silent) {
    printf("Done muting channels in %s!\n", wav_file_path);
  }

  return 0;
}